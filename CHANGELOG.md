# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## 2.0.0 (2021-06-16)


### ⚠ BREAKING CHANGES

* make docker registry login option explicit

### Features

* Add variable zammad_monitoring_internal_cidr ([1c3d9dd](https://gitlab.com/digiresilience/link/ansible-docker-zammad/commit/1c3d9dd0ad2d72b0da93b63488c7a94b176efb13))
* make docker registry login option explicit ([2747a10](https://gitlab.com/digiresilience/link/ansible-docker-zammad/commit/2747a10cf6e775472a5033e63d164320ae40bf10))


### Bug Fixes

* Ensure postgresql exporter can connect to zammad's postgres container ([5c17bd8](https://gitlab.com/digiresilience/link/ansible-docker-zammad/commit/5c17bd8278af2655095cd663f6edb320dfabdf52))
* monitoring token script can work without postgresql ([6d27292](https://gitlab.com/digiresilience/link/ansible-docker-zammad/commit/6d27292be82d8e36e7ff42ff22e71073dbb904f1))
* validate zammad_docker_registry variables ([ab2fff3](https://gitlab.com/digiresilience/link/ansible-docker-zammad/commit/ab2fff3f95566bdbcbdd537e4226035c6146c44d))
