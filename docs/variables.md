## Role Variables

* `zammad_domain`: `` - REQUIRED the domain where your zammad instance will be available


  ```yaml
  zammad_domain: zammad.example.com
  ```

* `sigarillo_domain`: `` - REQUIRED the domain where your sigarillo instance will be available



* `quepasa_domain`: `` - REQUIRED the domain where your quepasa instance will be available



* `zammad_letsencrypt_email`: `` - REQUIRED the email address used to register the LE certs, used for account recovery and renewal warnings



* `zammad_docker_restart_policy`: `unless-stopped` - by default the zammad docker containers will be restarted unless manually stopped



* `zammad_db_user`: `postgres` - the postgres user zammad will use



* `zammad_db_db`: `zammad_production` - the postgres database zammad will use



* `zammad_db_pass`: `` - REQUIRED the zammad postgres password



* `zammad_db_create`: `false` - whether or not the database should be created by zammad on first boot



* `zammad_docker_network`: `` - the name of the docker network, must already exist. if not specified, one will be created



* `zammad_memcached_size`: `256M` - the default memcached buffer size



* `zammad_sigarillo_secrets`: `` - REQUIRED a random string used to verify the cookie



* `zammad_sigarillo_db_uri`: `` - REQUIRED postgres uri for sigarillo



* `zammad_quepasa_signing_secret`: `` - REQUIRED a random string used to verify the cookie



* `zammad_quepasa_db_pass`: `` - REQUIRED postgres password for sigarillo zammad_quepasa_db: zammad_quepasa_db_user: zammad_quepasa_db_host: zammad_quepasa_db_port:



* `zammad_exporter_listen_address`: `` - The host address the exporter should listen on (should be an internal interface) required when monitoring is enabled.



* `zammad_monitoring_token`: `` - The monitoring token used to fetch zammad metrics. This will be set on zammad during setup and used by the zammad_exporter. required when monitoring is enabled.



* `zammad_monitoring_internal_cidr`: `` - The internal network used for monitoring services. required when monitoring is enabled.


  ```yaml
  zammad_monitoring_internal_cidr: 10.1.1.0/24
  ```

* `zammad_docker_registry_enabled`: `false` - whether or not to login to a container registry



* `zammad_docker_registry`: `` - optional registry to login to



* `zammad_docker_registry_username`: `` - username for the optional container registry



* `zammad_docker_registry_password`: `` - password for the optional container registry



* `zammad_es_enabled`: `false` - whether to enable elasticsearch



* `zammad_es_host`: `` - optional. the hostname of the elasticsearch instance



* `zammad_es_port`: `` - the port of the elasticsearch instance



* `zammad_es_schema`: `http` - the http/https schema to use when connecting. IS INSECURE BY DEFAULT



* `zammad_es_namespace`: `zammad` - the namespace to use in elasticsearch



* `zammad_es_reindex`: `true` - whether or not to reindex when starting



* `zammad_es_ssl_verify`: `true` - whether to verify the remote elasticsearch tls cert


