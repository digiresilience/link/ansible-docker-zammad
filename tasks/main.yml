---
- name: ensure variables are defined
  assert:
    that:
      - lookup('varnames', item)
      - lookup('vars', item) != none
      - lookup('vars', item) | length > 0
    fail_msg: Required variable {{ item }} is not well defined
    success_msg: Required variable {{ item }} is well defined
  loop:
    - zammad_domain
    - zammad_db_password

- name: ensure letsencrypt variables are defined
  assert:
    that:
      - lookup('varnames', item)
      - lookup('vars', item) != none
      - lookup('vars', item) | length > 0
    fail_msg: Required variable {{ item }} is not well defined
    success_msg: Required variable {{ item }} is well defined
  loop:
    - zammad_letsencrypt_email
  when: zammad_standalone

- name: ensure monitoring variables are defined
  assert:
    that:
      - lookup('varnames', item)
      - lookup('vars', item) != none
      - lookup('vars', item) | length > 0
    fail_msg: Required variable {{ item }} is not well defined
    success_msg: Required variable {{ item }} is well defined
  loop:
    - zammad_exporter_listen_address
    - zammad_monitoring_token
  when: zammad_monitoring_enabled

- name: ensure graphql variables are defined
  assert:
    that:
      - lookup('varnames', item)
      - lookup('vars', item) != none
      - lookup('vars', item) | length > 0
    fail_msg: Required variable {{ item }} is not well defined
    success_msg: Required variable {{ item }} is well defined
  loop:
    - zammad_graphql_cloudflare_access_audience
    - zammad_graphql_cloudflare_access_url
    - zammad_graphql_db_uri
    - zammad_graphql_domain
  when: zammad_graphql_enabled

- name: ensure registry variables are defined
  assert:
    that:
      - lookup('varnames', item)
      - lookup('vars', item) != none
      - lookup('vars', item) | length > 0
    fail_msg: Required variable {{ item }} is not well defined
    success_msg: Required variable {{ item }} is well defined
  loop:
    - zammad_docker_registry
    - zammad_docker_registry_password
    - zammad_docker_registry_username
  when: zammad_docker_registry_enabled

- name: ensure es variables are defined
  assert:
    that:
      - lookup('varnames', item)
      - lookup('vars', item) != none
      - lookup('vars', item) | length > 0
    fail_msg: Required variable {{ item }} is not well defined
    success_msg: Required variable {{ item }} is well defined
  loop:
    - zammad_es_host
    - zammad_es_schema
    - zammad_es_namespace
  when: zammad_es_enabled

- name: Set sysctl settings for elasticsearch
  sysctl:
    name: vm.max_map_count
    value: '262144'
    state: present
  when: zammad_standalone

- name: populate /etc/environment
  lineinfile:
    dest: "/etc/environment"
    state: present
    regexp: "^{{ item.key }}"
    line: "{{ item.key }}={{ item.value }}"
  with_items:
    - key: DOCKER_CONTENT_TRUST
      value: 1

- name: Setup zammad
  include_tasks: setup.yml
