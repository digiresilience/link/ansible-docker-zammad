---
- name: Create
  hosts: localhost
  connection: local
  gather_facts: false
  no_log: "{{ molecule_no_log }}"
  vars_files:
    - ./aws.yml
  tasks:
    - name: Create security group
      ec2_group:
        name: "{{ security_group_name }}"
        description: "{{ security_group_name }}"
        rules: "{{ security_group_rules }}"
        rules_egress: "{{ security_group_rules_egress }}"
        vpc_id: "{{ vpc_id }}"
        tags: "{{ molecule_yml.common_tags }}"

    - name: Test for presence of local keypair
      stat:
        path: "{{ keypair_path }}"
      register: keypair_local

    - name: Delete remote keypair
      ec2_key:
        name: "{{ keypair_name }}"
        state: absent
      when: not keypair_local.stat.exists

    - name: Create keypair
      ec2_key:
        name: "{{ keypair_name }}"
      register: keypair

    - name: Persist the keypair
      copy:
        dest: "{{ keypair_path }}"
        content: "{{ keypair.key.private_key }}"
        mode: 0600
      when: keypair.changed

    - name: Get the ec2 ami(s) by owner and name, if image not set
      ec2_ami_info:
        owners: "{{ item.image_owner }}"
        filters:
          name: "{{ item.image_name }}"
      loop: "{{ molecule_yml.platforms }}"
      when: item.image is not defined
      register: ami_facts

    - name: Create molecule instance(s)
      ec2_instance:
        name: "{{ item.name }}"
        state: running
        key_name: "{{ keypair_name }}"
        image_id: "{{ item.image
          if item.image is defined
          else (ami_facts.results[index].images | sort(attribute='creation_date', reverse=True))[0].image_id }}"
        instance_type: "{{ item.instance_type }}"
        vpc_subnet_id: "{{ item.vpc_subnet_id }}"
        security_group: "{{ security_group_name }}"
        instance_initiated_shutdown_behavior: terminate
        termination_protection: false
        tags: "{{ item.tags | combine(molecule_yml.common_tags) }}"
        # volumes:
        #   - device_name: /dev/nvme1n1
        #     ebs:
        #       volume_size: 40
        #       volume_type: gp2
        #       delete_on_termination: true
        # user_data: |
        #  #!/bin/bash
        #   echo "sudo halt" | at now + 120 min
        wait: true
        network:
          assign_public_ip: true
      register: server
      loop: "{{ molecule_yml.platforms }}"
      loop_control:
        index_var: index
      async: 7200
      poll: 0

    - name: Wait for instance(s) creation to complete
      async_status:
        jid: "{{ item.ansible_job_id }}"
      register: ec2_jobs
      until: ec2_jobs.finished
      retries: 300
      with_items: "{{ server.results }}"

    - debug:
        var: ec2_jobs.results

    # Mandatory configuration for Molecule to function.

    - name: Populate instance config dict
      set_fact:
        instance_conf_dict: {
          'instance': "{{ item.instances[0].tags.Name}}",
          'address': "{{ item.instances[0].network_interfaces[0].association.public_ip }}",
          'user': "{{ ssh_user }}",
          'port': "{{ ssh_port }}",
          'identity_file': "{{ keypair_path }}",
          'instance_ids': "{{ item.instance_ids }}",
          'become_method': "sudo"}
      with_items: "{{ ec2_jobs.results }}"
      register: instance_config_dict
      when: server.changed | bool

    - name: Convert instance config dict to a list
      set_fact:
        instance_conf: "{{ instance_config_dict.results | map(attribute='ansible_facts.instance_conf_dict') | list }}"
      when: server.changed | bool

    - name: Dump instance config
      copy:
        content: "{{ instance_conf | to_json | from_json | molecule_to_yaml | molecule_header }}"
        dest: "{{ molecule_instance_config }}"
      when: server.changed | bool

    - name: Wait for SSH
      wait_for:
        port: "{{ ssh_port }}"
        host: "{{ item.address }}"
        search_regex: SSH
        delay: 10
        timeout: 320
      with_items: "{{ lookup('file', molecule_instance_config) | molecule_from_yaml }}"
